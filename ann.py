# Classification template

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os


class AnnDeepLearning:
    """
    main problem: It´s used for linear regression problem and classification problem
    category: Supervisionate
    """

    def __init__(self, x_train, x_test, y_train, y_test):
        
        from keras.layers import Dense
        from keras.layers import Dropout
        import keras
        from keras.models import Sequential

        self.X_train = x_train
        self.X_test = x_test
        self.y_train = y_train
        self.y_test = y_test

        # Initialising the ANN
        self.classifier = Sequential()

    def build_classifier(self):

        # Adding input layer and the first hidden layer
        # hidden layer define the number of nodes
        # tip: average between the number of nodes of the input layer and output
        # nodes = independ variables
        self.classifier.add(Dense(output_dim=int((self.X_train.shape[1] + 1) / 2),
            #output_dim=6,
            init='uniform',  # randlomy initialize the weighs with uniform function
            activation='relu',  # activate function in the hidden layer (rectifier function)
            input_dim=int(self.X_train.shape[1])
        )
        )
        self.classifier.add(Dropout(  # It´s used to disable some layers, this is helps to avoid overfitting
            p=0.1  # Fraction of the input that you want to drop,
            # try 0.1, 0.2...until 0.4, to achieve the result
        ))

        # Adding the second hidden layer
        self.classifier.add(Dense(output_dim=int((self.X_train.shape[1] + 1) / 2),
            #output_dim=6,
            init='uniform',  # randlomy initialize the weighs with uniform function
            activation='relu',  # activate function in the hidden layer (rectifier function)
        )
        )
        self.classifier.add(Dropout(  # It´s used to disable some layers, this is helps to avoid overfitting
            p=0.1  # Fraction of the input that you want to drop,
            # try 0.1, 0.2...until 0.4, to achieve the result
        ))

        # Adding the output layer
        self.classifier.add(Dense(output_dim=1,
                                  init='uniform',  # randlomy initialize the weighs with uniform function
                                  activation='sigmoid',  # activate function in the hidden layer (sigmoid function)
                                  )
                            )

        # If the data has more than 2 categories (classes), the best function to choose is softmax and
        # the output_dim needs to receive the number of categorical data

        # Compiling the ANN
        self.classifier.compile(optimizer='adam',
                                # Algorithm to set up the values of weighs (stocastic gradient descent)
                                loss='binary_crossentropy',
                                # Specify the loss function (categorical_entropy for 3 or more classes
                                # Help to find the taxes (losses) to optimize the weigh values
                                metrics=['accuracy']
                                )

    def apply_test_base(self):

        '''
        TO DO: Corrigir o formato de entrada
        :param y_test:
        :return:
        '''

        # Fitting the ANN to training set
        self.classifier.fit(X_train, y_train, batch_size=10,  # number of rows to run each time
                            nb_epoch=100  # number of times the algorithm will execute
                            )

        # Predicting the Test set results
        y_pred = self.classifier.predict(X_test)
        y_pred = (y_pred > 0.5)

        from sklearn.metrics import confusion_matrix
        cm = confusion_matrix(y_test, y_pred)

        print(cm)

    def apply_confusion_matrix(self):
        # Making the Confusion Matrix
        from sklearn.metrics import confusion_matrix
        cm = confusion_matrix(self.y_test, self.y_pred)

        print(cm)

    def apply_cross_validation_ann(self, batch_size=10, nb_epoch=100):
        from keras.wrappers.scikit_learn import KerasClassifier
        from sklearn.model_selection import cross_val_score

        classifier = KerasClassifier(build_fn=self.build_classifier, batch_size=batch_size, nb_epoch=nb_epoch)
        accuracies = cross_val_score(estimator=classifier, X=self.X_train, y=self.y_train,
                                     cv=10,  # number of folds to aplly in X_train
                                     n_jobs=-1  # number of jobs, to do this cross validation in a parallel way"
                                     )

        for a in accuracies:
            print(a)

        print("Media da acuracia é {} e variancia {}".format(accuracies.mean(), accuracies.std()))

    def apply_grid_search_ann(self):

        from sklearn.model_selection import GridSearchCV

        parameters = {
            'batch_size': [25, 32],
            'nb_epoch': [100, 500],
            'optimizer': ['adam', 'rmsprop']
        }

        grid_search = GridSearchCV(estimator=self.classifier,
                                   param_grid=parameters,
                                   scoring='accuracy',
                                   cv=10)
        grid_search = grid_search.fit(self.X_train, self.y_train)
        best_parameters = grid_search.best_params_
        best_accuracy = grid_search.best_score_

        print("best_parameters: {}".format(best_parameters))
        print("best_accuracy: {}".format(best_accuracy))

    def home_work(self):
        """
        Homework
        Predict if the customer with the following informations will leave the bank:
        Geography: France
        Credit Score: 600
        Gender: Male
        Age: 40
        Tenure: 3
        Balance: 60000
        Number of Products: 2
        Has Credit Card: Yes
        Is Active Member: Yes
        Estimate Salary: 50000
        """
        new_customer = self.classifier.predict(sc.transform(np.array([[0, 0, 600, 1,
                                                                  40, 3, 60000,
                                                                  2, 1, 1, 50000]]
                                                                )
                                                       )
                                          )

        new_customer = (new_customer > 0.5)

        print(new_customer)


if __name__ == '__main__':
    
    file_path = os.path.join(os.getcwd(), 'secao_1 - ANN')
    file_path = os.path.join(file_path, 'Churn_Modelling.csv')
    
    # Importing the dataset
    dataset = pd.read_csv(file_path)
    X = dataset.iloc[:, 3:13].values
    y = dataset.iloc[:, 13].values

    # Encoding categorical data
    from sklearn.preprocessing import LabelEncoder, OneHotEncoder

    labelencoder_X_1 = LabelEncoder()
    X[:, 1] = labelencoder_X_1.fit_transform(X[:, 1])
    labelencoder_X_2 = LabelEncoder()
    X[:, 2] = labelencoder_X_2.fit_transform(X[:, 2])
    onehotencoder = OneHotEncoder(categorical_features=[1])
    X = onehotencoder.fit_transform(X).toarray()
    X = X[:, 1:]

    # Splitting the dataset into the Training set and Test set
    from sklearn.model_selection import train_test_split

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=0)

    # Feature Scaling
    from sklearn.preprocessing import StandardScaler

    sc = StandardScaler()
    X_train = sc.fit_transform(X_train)
    X_test = sc.transform(X_test)

    ####################################
    # Part 2 - Now let´s make the ANN  #
    ####################################

    ann = AnnDeepLearning(X_train, X_test, y_train, y_test)
    ann.build_classifier()
    ann.apply_cross_validation_ann()
